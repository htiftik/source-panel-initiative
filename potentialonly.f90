subroutine potential_line_aux(x,z,x1,x2,sigma,u,w,phi)
    real, parameter :: pi = 4.0*atan(1.0)
    real, intent(in) :: x,z,x1,x2,sigma
    real, intent(out) :: u,w,phi

    real :: theta1,theta2,r1,r2

    ! auxiliary locals
    theta1 = atan2(z,(x-x1))
    theta2 = atan2(z,(x-x2))

    r1 = sqrt((x-x1)**2+z**2)
    r2 = sqrt((x-x2)**2+z**2)

    ! potential
    phi = (sigma/4.0/PI)*((x-x1)*log(r1**2)-(x-x2)*log(r2**2)+2*z*(theta2-theta1))

    ! velocities ... added to compare with potential derivative of TAPENADE
    u = (sigma/2.0/PI)*log(r1/r2)
    w = (sigma/2.0/PI)*(theta2-theta1)
end subroutine potential_line_aux


subroutine potential_line(x,z,x1,z1,x2,z2,sigma,u,w,phi)
    real, parameter :: pi = 4.0*atan(1.0)
    real, intent(in) :: x,z,x1,z1,x2,z2,sigma
    real, intent(out) :: u,w,phi

    real :: dx,dz,e1x,e1z,f1x,f1z,L
    real :: locx, locz, uloc, wloc

    dx = x2-x1
    dz = z2-z1

    L = sqrt(dx**2+dz**2)
    e1x = dx/L
    e1z = dz/L

    f1x = -e1z
    f1z = +e1x

    locx = (x-x1)*e1x+(z-z1)*e1z
    locz = (x-x1)*f1x+(z-z1)*f1z


    call potential_line_aux(locx,locz,0.0,L,sigma,uloc,wloc,phi)

    u = uloc*e1x+wloc*f1x
    w = uloc*e1z+wloc*f1z

end subroutine potential_line
