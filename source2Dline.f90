subroutine source2DlineAux(x,z,x1,x2,sigma,u,w,phi)
    real, parameter :: pi = 4.0*atan(1.0)
    real, intent(in) :: x,z,x1,x2,sigma
    real, intent(out) :: u,w,phi

    real :: theta1,theta2,r1,r2

    theta1 = atan2(z,(x-x1))
    theta2 = atan2(z,(x-x2))

    r1 = sqrt((x-x1)**2+z**2)
    r2 = sqrt((x-x2)**2+z**2)

    phi = (sigma/4.0/PI)*((x-x1)*log(r1**2)-(x-x2)**2*log(r2**2)+2*z*(theta2-theta1))
    u = (sigma/2.0/PI)*log(r1/r2)
    w = (sigma/2.0/PI)*(theta2-theta1)
end subroutine source2DlineAux

subroutine source2Dline(x1,z1,x2,z2,x,z,sigma,u,w,phi)
    real, intent(in) :: x1,z1,x2,z2,x,z,sigma
    real, intent(out) :: u,w,phi

    real :: L, dx,dz,ex,ez,fx,fz,uloc,wloc

    dx = x2-x1
    dz = z2-z1

    L = sqrt(dx**2+dz**2)

    ex = dx/L
    ez = dz/L

    fx = -ez
    fz = +ex

    if (.true.) then
        ! subroutine source2DlineAux(x,z,x1,x2,sigma,u,w,phi)
        call source2DlineAux( (x-x1)*ex+(z-z1)*ez, (x-x1)*fx+(z-z1)*fz, 0.0, L, sigma, uloc, wloc, phi)
    else
        ! subroutine source2DlineAux(x,z,x1,x2,sigma,u,w,phi)
        call source2DlineAux( dot_product([x-x1,z-z1],[ex,ez]), dot_product([x-x1,z-z1],[fx,fz]), 0.0, L, sigma, uloc, wloc, phi)
    end if


    u = uloc*ex+wloc*fx
    w = uloc*ez+wloc*fz

end subroutine source2Dline
