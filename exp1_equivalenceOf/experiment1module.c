/* File: experiment1module.c
 * This file is auto-generated with f2py (version:2).
 * f2py is a Fortran to Python Interface Generator (FPIG), Second Edition,
 * written by Pearu Peterson <pearu@cens.ioc.ee>.
 * See http://cens.ioc.ee/projects/f2py2e/
 * Generation date: Sun Oct 26 08:51:18 2014
 * $Revision:$
 * $Date:$
 * Do not edit this file directly unless you know what you are doing!!!
 */
#ifdef __cplusplus
extern "C" {
#endif

/*********************** See f2py2e/cfuncs.py: includes ***********************/
#include "Python.h"
#include "fortranobject.h"
/*need_includes0*/

/**************** See f2py2e/rules.py: mod_rules['modulebody'] ****************/
static PyObject *experiment1_error;
static PyObject *experiment1_module;

/*********************** See f2py2e/cfuncs.py: typedefs ***********************/
/*need_typedefs*/

/****************** See f2py2e/cfuncs.py: typedefs_generated ******************/
/*need_typedefs_generated*/

/********************** See f2py2e/cfuncs.py: cppmacros **********************/
#if defined(PREPEND_FORTRAN)
#if defined(NO_APPEND_FORTRAN)
#if defined(UPPERCASE_FORTRAN)
#define F_FUNC(f,F) _##F
#else
#define F_FUNC(f,F) _##f
#endif
#else
#if defined(UPPERCASE_FORTRAN)
#define F_FUNC(f,F) _##F##_
#else
#define F_FUNC(f,F) _##f##_
#endif
#endif
#else
#if defined(NO_APPEND_FORTRAN)
#if defined(UPPERCASE_FORTRAN)
#define F_FUNC(f,F) F
#else
#define F_FUNC(f,F) f
#endif
#else
#if defined(UPPERCASE_FORTRAN)
#define F_FUNC(f,F) F##_
#else
#define F_FUNC(f,F) f##_
#endif
#endif
#endif
#if defined(UNDERSCORE_G77)
#define F_FUNC_US(f,F) F_FUNC(f##_,F##_)
#else
#define F_FUNC_US(f,F) F_FUNC(f,F)
#endif

#ifdef DEBUGCFUNCS
#define CFUNCSMESS(mess) fprintf(stderr,"debug-capi:"mess);
#define CFUNCSMESSPY(mess,obj) CFUNCSMESS(mess) \
  PyObject_Print((PyObject *)obj,stderr,Py_PRINT_RAW);\
  fprintf(stderr,"\n");
#else
#define CFUNCSMESS(mess)
#define CFUNCSMESSPY(mess,obj)
#endif

#ifndef max
#define max(a,b) ((a > b) ? (a) : (b))
#endif
#ifndef min
#define min(a,b) ((a < b) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a,b) ((a > b) ? (a) : (b))
#endif
#ifndef MIN
#define MIN(a,b) ((a < b) ? (a) : (b))
#endif


/************************ See f2py2e/cfuncs.py: cfuncs ************************/
static int double_from_pyobj(double* v,PyObject *obj,const char *errmess) {
  PyObject* tmp = NULL;
  if (PyFloat_Check(obj)) {
#ifdef __sgi
    *v = PyFloat_AsDouble(obj);
#else
    *v = PyFloat_AS_DOUBLE(obj);
#endif
    return 1;
  }
  tmp = PyNumber_Float(obj);
  if (tmp) {
#ifdef __sgi
    *v = PyFloat_AsDouble(tmp);
#else
    *v = PyFloat_AS_DOUBLE(tmp);
#endif
    Py_DECREF(tmp);
    return 1;
  }
  if (PyComplex_Check(obj))
    tmp = PyObject_GetAttrString(obj,"real");
  else if (PyString_Check(obj) || PyUnicode_Check(obj))
    /*pass*/;
  else if (PySequence_Check(obj))
    tmp = PySequence_GetItem(obj,0);
  if (tmp) {
    PyErr_Clear();
    if (double_from_pyobj(v,tmp,errmess)) {Py_DECREF(tmp); return 1;}
    Py_DECREF(tmp);
  }
  {
    PyObject* err = PyErr_Occurred();
    if (err==NULL) err = experiment1_error;
    PyErr_SetString(err,errmess);
  }
  return 0;
}

static int float_from_pyobj(float* v,PyObject *obj,const char *errmess) {
  double d=0.0;
  if (double_from_pyobj(&d,obj,errmess)) {
    *v = (float)d;
    return 1;
  }
  return 0;
}


/********************* See f2py2e/cfuncs.py: userincludes *********************/
/*need_userincludes*/

/********************* See f2py2e/capi_rules.py: usercode *********************/


/* See f2py2e/rules.py */
extern void F_FUNC_US(potential_line_aux,POTENTIAL_LINE_AUX)(float*,float*,float*,float*,float*,float*,float*,float*);
extern void F_FUNC_US(POTENTIAL_LINE_AUX_D,POTENTIAL_LINE_AUX_D)(float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*);
/*eof externroutines*/

/******************** See f2py2e/capi_rules.py: usercode1 ********************/


/******************* See f2py2e/cb_rules.py: buildcallback *******************/
/*need_callbacks*/

/*********************** See f2py2e/rules.py: buildapi ***********************/

/***************************** potential_line_aux *****************************/
static char doc_f2py_rout_experiment1_potential_line_aux[] = "\
u,w,phi = potential_line_aux(x,z,x1,x2,sigma)\n\nWrapper for ``potential_line_aux``.\
\n\nParameters\n----------\n"
"x : input float\n"
"z : input float\n"
"x1 : input float\n"
"x2 : input float\n"
"sigma : input float\n"
"\nReturns\n-------\n"
"u : float\n"
"w : float\n"
"phi : float";
/* extern void F_FUNC_US(potential_line_aux,POTENTIAL_LINE_AUX)(float*,float*,float*,float*,float*,float*,float*,float*); */
static PyObject *f2py_rout_experiment1_potential_line_aux(const PyObject *capi_self,
                           PyObject *capi_args,
                           PyObject *capi_keywds,
                           void (*f2py_func)(float*,float*,float*,float*,float*,float*,float*,float*)) {
  PyObject * volatile capi_buildvalue = NULL;
  volatile int f2py_success = 1;
/*decl*/

  float x = 0;
  PyObject *x_capi = Py_None;
  float z = 0;
  PyObject *z_capi = Py_None;
  float x1 = 0;
  PyObject *x1_capi = Py_None;
  float x2 = 0;
  PyObject *x2_capi = Py_None;
  float sigma = 0;
  PyObject *sigma_capi = Py_None;
  float u = 0;
  float w = 0;
  float phi = 0;
  static char *capi_kwlist[] = {"x","z","x1","x2","sigma",NULL};

/*routdebugenter*/
#ifdef F2PY_REPORT_ATEXIT
f2py_start_clock();
#endif
  if (!PyArg_ParseTupleAndKeywords(capi_args,capi_keywds,\
    "OOOOO:experiment1.potential_line_aux",\
    capi_kwlist,&x_capi,&z_capi,&x1_capi,&x2_capi,&sigma_capi))
    return NULL;
/*frompyobj*/
  /* Processing variable phi */
  /* Processing variable z */
    f2py_success = float_from_pyobj(&z,z_capi,"experiment1.potential_line_aux() 2nd argument (z) can't be converted to float");
  if (f2py_success) {
  /* Processing variable x1 */
    f2py_success = float_from_pyobj(&x1,x1_capi,"experiment1.potential_line_aux() 3rd argument (x1) can't be converted to float");
  if (f2py_success) {
  /* Processing variable u */
  /* Processing variable w */
  /* Processing variable x2 */
    f2py_success = float_from_pyobj(&x2,x2_capi,"experiment1.potential_line_aux() 4th argument (x2) can't be converted to float");
  if (f2py_success) {
  /* Processing variable x */
    f2py_success = float_from_pyobj(&x,x_capi,"experiment1.potential_line_aux() 1st argument (x) can't be converted to float");
  if (f2py_success) {
  /* Processing variable sigma */
    f2py_success = float_from_pyobj(&sigma,sigma_capi,"experiment1.potential_line_aux() 5th argument (sigma) can't be converted to float");
  if (f2py_success) {
/*end of frompyobj*/
#ifdef F2PY_REPORT_ATEXIT
f2py_start_call_clock();
#endif
/*callfortranroutine*/
        (*f2py_func)(&x,&z,&x1,&x2,&sigma,&u,&w,&phi);
if (PyErr_Occurred())
  f2py_success = 0;
#ifdef F2PY_REPORT_ATEXIT
f2py_stop_call_clock();
#endif
/*end of callfortranroutine*/
    if (f2py_success) {
/*pyobjfrom*/
/*end of pyobjfrom*/
    CFUNCSMESS("Building return value.\n");
    capi_buildvalue = Py_BuildValue("fff",u,w,phi);
/*closepyobjfrom*/
/*end of closepyobjfrom*/
    } /*if (f2py_success) after callfortranroutine*/
/*cleanupfrompyobj*/
  } /*if (f2py_success) of sigma*/
  /* End of cleaning variable sigma */
  } /*if (f2py_success) of x*/
  /* End of cleaning variable x */
  } /*if (f2py_success) of x2*/
  /* End of cleaning variable x2 */
  /* End of cleaning variable w */
  /* End of cleaning variable u */
  } /*if (f2py_success) of x1*/
  /* End of cleaning variable x1 */
  } /*if (f2py_success) of z*/
  /* End of cleaning variable z */
  /* End of cleaning variable phi */
/*end of cleanupfrompyobj*/
  if (capi_buildvalue == NULL) {
/*routdebugfailure*/
  } else {
/*routdebugleave*/
  }
  CFUNCSMESS("Freeing memory.\n");
/*freemem*/
#ifdef F2PY_REPORT_ATEXIT
f2py_stop_clock();
#endif
  return capi_buildvalue;
}
/************************* end of potential_line_aux *************************/

/**************************** POTENTIAL_LINE_AUX_D ****************************/
static char doc_f2py_rout_experiment1_POTENTIAL_LINE_AUX_D[] = "\
POTENTIAL_LINE_AUX_D(x,xd,z,zd,x1,x1d,x2,x2d,sigma,sigmad,u,ud,w,wd,phi,phid)\n\nWrapper for ``POTENTIAL_LINE_AUX_D``.\
\n\nParameters\n----------\n"
"x : input float\n"
"xd : input float\n"
"z : input float\n"
"zd : input float\n"
"x1 : input float\n"
"x1d : input float\n"
"x2 : input float\n"
"x2d : input float\n"
"sigma : input float\n"
"sigmad : input float\n"
"u : input float\n"
"ud : input float\n"
"w : input float\n"
"wd : input float\n"
"phi : input float\n"
"phid : input float";
/* extern void F_FUNC_US(POTENTIAL_LINE_AUX_D,POTENTIAL_LINE_AUX_D)(float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*); */
static PyObject *f2py_rout_experiment1_POTENTIAL_LINE_AUX_D(const PyObject *capi_self,
                           PyObject *capi_args,
                           PyObject *capi_keywds,
                           void (*f2py_func)(float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*,float*)) {
  PyObject * volatile capi_buildvalue = NULL;
  volatile int f2py_success = 1;
/*decl*/

  float x = 0;
  PyObject *x_capi = Py_None;
  float xd = 0;
  PyObject *xd_capi = Py_None;
  float z = 0;
  PyObject *z_capi = Py_None;
  float zd = 0;
  PyObject *zd_capi = Py_None;
  float x1 = 0;
  PyObject *x1_capi = Py_None;
  float x1d = 0;
  PyObject *x1d_capi = Py_None;
  float x2 = 0;
  PyObject *x2_capi = Py_None;
  float x2d = 0;
  PyObject *x2d_capi = Py_None;
  float sigma = 0;
  PyObject *sigma_capi = Py_None;
  float sigmad = 0;
  PyObject *sigmad_capi = Py_None;
  float u = 0;
  PyObject *u_capi = Py_None;
  float ud = 0;
  PyObject *ud_capi = Py_None;
  float w = 0;
  PyObject *w_capi = Py_None;
  float wd = 0;
  PyObject *wd_capi = Py_None;
  float phi = 0;
  PyObject *phi_capi = Py_None;
  float phid = 0;
  PyObject *phid_capi = Py_None;
  static char *capi_kwlist[] = {"x","xd","z","zd","x1","x1d","x2","x2d","sigma","sigmad","u","ud","w","wd","phi","phid",NULL};

/*routdebugenter*/
#ifdef F2PY_REPORT_ATEXIT
f2py_start_clock();
#endif
  if (!PyArg_ParseTupleAndKeywords(capi_args,capi_keywds,\
    "OOOOOOOOOOOOOOOO:experiment1.POTENTIAL_LINE_AUX_D",\
    capi_kwlist,&x_capi,&xd_capi,&z_capi,&zd_capi,&x1_capi,&x1d_capi,&x2_capi,&x2d_capi,&sigma_capi,&sigmad_capi,&u_capi,&ud_capi,&w_capi,&wd_capi,&phi_capi,&phid_capi))
    return NULL;
/*frompyobj*/
  /* Processing variable phi */
    f2py_success = float_from_pyobj(&phi,phi_capi,"experiment1.POTENTIAL_LINE_AUX_D() 15th argument (phi) can't be converted to float");
  if (f2py_success) {
  /* Processing variable w */
    f2py_success = float_from_pyobj(&w,w_capi,"experiment1.POTENTIAL_LINE_AUX_D() 13rd argument (w) can't be converted to float");
  if (f2py_success) {
  /* Processing variable zd */
    f2py_success = float_from_pyobj(&zd,zd_capi,"experiment1.POTENTIAL_LINE_AUX_D() 4th argument (zd) can't be converted to float");
  if (f2py_success) {
  /* Processing variable x1d */
    f2py_success = float_from_pyobj(&x1d,x1d_capi,"experiment1.POTENTIAL_LINE_AUX_D() 6th argument (x1d) can't be converted to float");
  if (f2py_success) {
  /* Processing variable xd */
    f2py_success = float_from_pyobj(&xd,xd_capi,"experiment1.POTENTIAL_LINE_AUX_D() 2nd argument (xd) can't be converted to float");
  if (f2py_success) {
  /* Processing variable wd */
    f2py_success = float_from_pyobj(&wd,wd_capi,"experiment1.POTENTIAL_LINE_AUX_D() 14th argument (wd) can't be converted to float");
  if (f2py_success) {
  /* Processing variable ud */
    f2py_success = float_from_pyobj(&ud,ud_capi,"experiment1.POTENTIAL_LINE_AUX_D() 12nd argument (ud) can't be converted to float");
  if (f2py_success) {
  /* Processing variable x2 */
    f2py_success = float_from_pyobj(&x2,x2_capi,"experiment1.POTENTIAL_LINE_AUX_D() 7th argument (x2) can't be converted to float");
  if (f2py_success) {
  /* Processing variable x1 */
    f2py_success = float_from_pyobj(&x1,x1_capi,"experiment1.POTENTIAL_LINE_AUX_D() 5th argument (x1) can't be converted to float");
  if (f2py_success) {
  /* Processing variable phid */
    f2py_success = float_from_pyobj(&phid,phid_capi,"experiment1.POTENTIAL_LINE_AUX_D() 16th argument (phid) can't be converted to float");
  if (f2py_success) {
  /* Processing variable x2d */
    f2py_success = float_from_pyobj(&x2d,x2d_capi,"experiment1.POTENTIAL_LINE_AUX_D() 8th argument (x2d) can't be converted to float");
  if (f2py_success) {
  /* Processing variable z */
    f2py_success = float_from_pyobj(&z,z_capi,"experiment1.POTENTIAL_LINE_AUX_D() 3rd argument (z) can't be converted to float");
  if (f2py_success) {
  /* Processing variable u */
    f2py_success = float_from_pyobj(&u,u_capi,"experiment1.POTENTIAL_LINE_AUX_D() 11st argument (u) can't be converted to float");
  if (f2py_success) {
  /* Processing variable sigmad */
    f2py_success = float_from_pyobj(&sigmad,sigmad_capi,"experiment1.POTENTIAL_LINE_AUX_D() 10th argument (sigmad) can't be converted to float");
  if (f2py_success) {
  /* Processing variable x */
    f2py_success = float_from_pyobj(&x,x_capi,"experiment1.POTENTIAL_LINE_AUX_D() 1st argument (x) can't be converted to float");
  if (f2py_success) {
  /* Processing variable sigma */
    f2py_success = float_from_pyobj(&sigma,sigma_capi,"experiment1.POTENTIAL_LINE_AUX_D() 9th argument (sigma) can't be converted to float");
  if (f2py_success) {
/*end of frompyobj*/
#ifdef F2PY_REPORT_ATEXIT
f2py_start_call_clock();
#endif
/*callfortranroutine*/
        (*f2py_func)(&x,&xd,&z,&zd,&x1,&x1d,&x2,&x2d,&sigma,&sigmad,&u,&ud,&w,&wd,&phi,&phid);
if (PyErr_Occurred())
  f2py_success = 0;
#ifdef F2PY_REPORT_ATEXIT
f2py_stop_call_clock();
#endif
/*end of callfortranroutine*/
    if (f2py_success) {
/*pyobjfrom*/
/*end of pyobjfrom*/
    CFUNCSMESS("Building return value.\n");
    capi_buildvalue = Py_BuildValue("");
/*closepyobjfrom*/
/*end of closepyobjfrom*/
    } /*if (f2py_success) after callfortranroutine*/
/*cleanupfrompyobj*/
  } /*if (f2py_success) of sigma*/
  /* End of cleaning variable sigma */
  } /*if (f2py_success) of x*/
  /* End of cleaning variable x */
  } /*if (f2py_success) of sigmad*/
  /* End of cleaning variable sigmad */
  } /*if (f2py_success) of u*/
  /* End of cleaning variable u */
  } /*if (f2py_success) of z*/
  /* End of cleaning variable z */
  } /*if (f2py_success) of x2d*/
  /* End of cleaning variable x2d */
  } /*if (f2py_success) of phid*/
  /* End of cleaning variable phid */
  } /*if (f2py_success) of x1*/
  /* End of cleaning variable x1 */
  } /*if (f2py_success) of x2*/
  /* End of cleaning variable x2 */
  } /*if (f2py_success) of ud*/
  /* End of cleaning variable ud */
  } /*if (f2py_success) of wd*/
  /* End of cleaning variable wd */
  } /*if (f2py_success) of xd*/
  /* End of cleaning variable xd */
  } /*if (f2py_success) of x1d*/
  /* End of cleaning variable x1d */
  } /*if (f2py_success) of zd*/
  /* End of cleaning variable zd */
  } /*if (f2py_success) of w*/
  /* End of cleaning variable w */
  } /*if (f2py_success) of phi*/
  /* End of cleaning variable phi */
/*end of cleanupfrompyobj*/
  if (capi_buildvalue == NULL) {
/*routdebugfailure*/
  } else {
/*routdebugleave*/
  }
  CFUNCSMESS("Freeing memory.\n");
/*freemem*/
#ifdef F2PY_REPORT_ATEXIT
f2py_stop_clock();
#endif
  return capi_buildvalue;
}
/************************ end of POTENTIAL_LINE_AUX_D ************************/
/*eof body*/

/******************* See f2py2e/f90mod_rules.py: buildhooks *******************/
/*need_f90modhooks*/

/************** See f2py2e/rules.py: module_rules['modulebody'] **************/

/******************* See f2py2e/common_rules.py: buildhooks *******************/

/*need_commonhooks*/

/**************************** See f2py2e/rules.py ****************************/

static FortranDataDef f2py_routine_defs[] = {
  {"potential_line_aux",-1,{{-1}},0,(char *)F_FUNC_US(potential_line_aux,POTENTIAL_LINE_AUX),(f2py_init_func)f2py_rout_experiment1_potential_line_aux,doc_f2py_rout_experiment1_potential_line_aux},
  {"POTENTIAL_LINE_AUX_D",-1,{{-1}},0,(char *)F_FUNC_US(POTENTIAL_LINE_AUX_D,POTENTIAL_LINE_AUX_D),(f2py_init_func)f2py_rout_experiment1_POTENTIAL_LINE_AUX_D,doc_f2py_rout_experiment1_POTENTIAL_LINE_AUX_D},

/*eof routine_defs*/
  {NULL}
};

static PyMethodDef f2py_module_methods[] = {

  {NULL,NULL}
};

#if PY_VERSION_HEX >= 0x03000000
static struct PyModuleDef moduledef = {
  PyModuleDef_HEAD_INIT,
  "experiment1",
  NULL,
  -1,
  f2py_module_methods,
  NULL,
  NULL,
  NULL,
  NULL
};
#endif

#if PY_VERSION_HEX >= 0x03000000
#define RETVAL m
PyMODINIT_FUNC PyInit_experiment1(void) {
#else
#define RETVAL
PyMODINIT_FUNC initexperiment1(void) {
#endif
  int i;
  PyObject *m,*d, *s;
#if PY_VERSION_HEX >= 0x03000000
  m = experiment1_module = PyModule_Create(&moduledef);
#else
  m = experiment1_module = Py_InitModule("experiment1", f2py_module_methods);
#endif
  Py_TYPE(&PyFortran_Type) = &PyType_Type;
  import_array();
  if (PyErr_Occurred())
    {PyErr_SetString(PyExc_ImportError, "can't initialize module experiment1 (failed to import numpy)"); return RETVAL;}
  d = PyModule_GetDict(m);
  s = PyString_FromString("$Revision: $");
  PyDict_SetItemString(d, "__version__", s);
#if PY_VERSION_HEX >= 0x03000000
  s = PyUnicode_FromString(
#else
  s = PyString_FromString(
#endif
    "This module 'experiment1' is auto-generated with f2py (version:2).\nFunctions:\n"
"  u,w,phi = potential_line_aux(x,z,x1,x2,sigma)\n"
"  POTENTIAL_LINE_AUX_D(x,xd,z,zd,x1,x1d,x2,x2d,sigma,sigmad,u,ud,w,wd,phi,phid)\n"
".");
  PyDict_SetItemString(d, "__doc__", s);
  experiment1_error = PyErr_NewException ("experiment1.error", NULL, NULL);
  Py_DECREF(s);
  for(i=0;f2py_routine_defs[i].name!=NULL;i++)
    PyDict_SetItemString(d, f2py_routine_defs[i].name,PyFortranObject_NewAsAttr(&f2py_routine_defs[i]));


/*eof initf2pywraphooks*/
/*eof initf90modhooks*/

/*eof initcommonhooks*/


#ifdef F2PY_REPORT_ATEXIT
  if (! PyErr_Occurred())
    on_exit(f2py_report_on_exit,(void*)"experiment1");
#endif

  return RETVAL;
}
#ifdef __cplusplus
}
#endif
