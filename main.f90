program main
    implicit none

    !external source2DlineAux
    !external SOURCE2DLINEAUX_D

    REAL :: x, z, x1, x2, sigma
    REAL :: u, w, phi
    REAL :: xd, zd, x1d, x2d, sigmad
    REAL :: ud, wd, phid

    x = 1.0
    z = 2.0
    x1 = -1.0
    x2 = +1.0
    sigma = 1.0

    call source2DlineAux(x,z,x1,x2,sigma,u,w,phi)

    write(*,*) 'original function results'
    write(6,'(3(A3,F6.2,X))') 'U ', u, 'W ', w, 'PHI', phi

    xd = 0.0
    zd = 0.0

    x1d = 1.0
    x2d = 1.0
    sigmad = 0.0



    call SOURCE2DLINEAUX_D(x, xd, z, zd, x1, x1d, x2, x2d, sigma, sigmad, u, ud, w, wd, phi, phid)
    write(*,*) 'tapenade generated function results'
    write(6,'(3(A3,F6.2,X))') 'U ', u, 'W ', w, 'PHI', phi
    write(6,'(3(A3,F6.2,X))') 'dU ', ud, 'dW ', wd, 'dPHI', phid


end program main

