!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.10 (r5363) -  9 Sep 2014 09:54
!
!  Differentiation of potential_line in forward (tangent) mode:
!   variations   of useful results: u w phi
!   with respect to varying inputs: x z sigma z1 z2 x1 x2
!   RW status of diff variables: u:out w:out x:in z:in sigma:in
!                z1:in z2:in phi:out x1:in x2:in
SUBROUTINE POTENTIAL_LINE_D(x, xd, z, zd, x1, x1d, z1, z1d, x2, x2d, z2&
& , z2d, sigma, sigmad, u, ud, w, wd, phi, phid)
  IMPLICIT NONE
  REAL, PARAMETER :: pi=4.0*ATAN(1.0)
  REAL, INTENT(IN) :: x, z, x1, z1, x2, z2, sigma
  REAL, INTENT(IN) :: xd, zd, x1d, z1d, x2d, z2d, sigmad
  REAL, INTENT(OUT) :: u, w, phi
  REAL, INTENT(OUT) :: ud, wd, phid
  REAL :: dx, dz, e1x, e1z, f1x, f1z, l
  REAL :: dxd, dzd, e1xd, e1zd, f1xd, f1zd, ld
  REAL :: locx, locz, uloc, wloc
  REAL :: locxd, loczd, ulocd, wlocd
  INTRINSIC ATAN
  INTRINSIC SQRT
  REAL :: arg1
  REAL :: arg1d
  dxd = x2d - x1d
  dx = x2 - x1
  dzd = z2d - z1d
  dz = z2 - z1
  arg1d = 2*dx*dxd + 2*dz*dzd
  arg1 = dx**2 + dz**2
  IF (arg1 .EQ. 0.0) THEN
    ld = 0.0
  ELSE
    ld = arg1d/(2.0*SQRT(arg1))
  END IF
  l = SQRT(arg1)
  e1xd = (dxd*l-dx*ld)/l**2
  e1x = dx/l
  e1zd = (dzd*l-dz*ld)/l**2
  e1z = dz/l
  f1xd = -e1zd
  f1x = -e1z
  f1zd = e1xd
  f1z = e1x
  locxd = (xd-x1d)*e1x + (x-x1)*e1xd + (zd-z1d)*e1z + (z-z1)*e1zd
  locx = (x-x1)*e1x + (z-z1)*e1z
  loczd = (xd-x1d)*f1x + (x-x1)*f1xd + (zd-z1d)*f1z + (z-z1)*f1zd
  locz = (x-x1)*f1x + (z-z1)*f1z
  CALL POTENTIAL_LINE_AUX_D(locx, locxd, locz, loczd, 0.0, l, ld, sigma&
&                     , sigmad, uloc, ulocd, wloc, wlocd, phi, phid)
  ud = ulocd*e1x + uloc*e1xd + wlocd*f1x + wloc*f1xd
  u = uloc*e1x + wloc*f1x
  wd = ulocd*e1z + uloc*e1zd + wlocd*f1z + wloc*f1zd
  w = uloc*e1z + wloc*f1z
END SUBROUTINE POTENTIAL_LINE_D
